// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package filterstring

import (
	"fmt"
	"time"

	"github.com/alecthomas/participle"
	"github.com/alecthomas/participle/lexer"
	"github.com/alecthomas/participle/lexer/ebnf"

	"gitlab.com/bichon-project/bichon/model"
)

type BinaryOperator int

const (
	BinaryOperatorBoth BinaryOperator = iota
	BinaryOperatorEither
)

var binaryOperatorMap = map[string]BinaryOperator{
	"&": BinaryOperatorBoth,
	"|": BinaryOperatorEither,
}

func (o *BinaryOperator) Capture(s []string) error {
	val, ok := binaryOperatorMap[s[0]]
	if !ok {
		return fmt.Errorf("Unknown binary operator '%s'", s[0])
	}
	*o = val
	return nil
}

type UnaryOperator int

const (
	UnaryOperatorReject UnaryOperator = iota
)

var unaryOperatorMap = map[string]UnaryOperator{
	"!": UnaryOperatorReject,
}

func (o *UnaryOperator) Capture(s []string) error {
	val, ok := unaryOperatorMap[s[0]]
	if !ok {
		return fmt.Errorf("Unknown unary operator '%s'", s[0])
	}
	*o = val
	return nil
}

type Field int

const (
	FieldProject Field = iota
	FieldSubmitterRealName
	FieldSubmitterUserName
	FieldAge
	FieldActivity
	FieldState
	FieldMetadataStatus
)

var fieldMap = map[string]Field{
	"p": FieldProject,
	"u": FieldSubmitterUserName,
	"r": FieldSubmitterRealName,
	"a": FieldAge,
	"t": FieldActivity,
	"s": FieldState,
	"m": FieldMetadataStatus,
}

func (f *Field) Capture(str []string) error {
	val, ok := fieldMap[str[0]]
	if !ok {
		return fmt.Errorf("Unknown field '%s'", str[0])
	}
	*f = val
	return nil
}

type TimeUnit int

const (
	TimeUnitYear TimeUnit = iota
	TimeUnitMonth
	TimeUnitWeek
	TimeUnitDay
	TimeUnitHour
	TimeUnitMinute
	TimeUnitSecond
)

var timeunitMap = map[string]TimeUnit{
	"y": TimeUnitYear,
	"o": TimeUnitMonth,
	"w": TimeUnitWeek,
	"d": TimeUnitDay,
	"h": TimeUnitHour,
	"m": TimeUnitMinute,
	"s": TimeUnitSecond,

	"year":   TimeUnitYear,
	"month":  TimeUnitMonth,
	"week":   TimeUnitWeek,
	"day":    TimeUnitDay,
	"hour":   TimeUnitHour,
	"minute": TimeUnitMinute,
	"second": TimeUnitSecond,

	"mon": TimeUnitMonth,
	"min": TimeUnitMinute,
	"sec": TimeUnitSecond,
}

var timeunitDuration = map[TimeUnit]time.Duration{
	TimeUnitYear:   time.Hour * 24 * 365,
	TimeUnitMonth:  time.Hour * 24 * 31,
	TimeUnitWeek:   time.Hour * 24 * 7,
	TimeUnitDay:    time.Hour * 24,
	TimeUnitHour:   time.Hour,
	TimeUnitMinute: time.Minute,
	TimeUnitSecond: time.Second,
}

func (f *TimeUnit) Capture(str []string) error {
	val, ok := timeunitMap[str[0]]
	if !ok {
		return fmt.Errorf("Unknown time unit '%s'", str[0])
	}
	*f = val
	return nil
}

type State int

const (
	StateOpened State = iota
	StateClosed
	StateMerged
	StateLocked
)

var stateMap = map[string]State{
	"o": StateOpened,
	"c": StateClosed,
	"m": StateMerged,
	"l": StateLocked,

	"open":   StateOpened,
	"closed": StateClosed,
	"merged": StateMerged,
	"locked": StateLocked,
}

func (s *State) Capture(str []string) error {
	val, ok := stateMap[str[0]]
	if !ok {
		return fmt.Errorf("Unknown state '%s'", str[0])
	}
	*s = val
	return nil
}

func (s *State) AsState() model.MergeReqState {
	switch *s {
	case StateOpened:
		return model.STATE_OPENED
	case StateClosed:
		return model.STATE_CLOSED
	case StateMerged:
		return model.STATE_MERGED
	case StateLocked:
		return model.STATE_LOCKED
	}
	panic("Unexpected state")
}

type Status int

const (
	StatusNew Status = iota
	StatusUpdated
	StatusOld
	StatusRead
)

var statusMap = map[string]Status{
	"n": StatusNew,
	"u": StatusUpdated,
	"o": StatusOld,
	"r": StatusRead,

	"new":     StatusNew,
	"updated": StatusUpdated,
	"old":     StatusOld,
	"read":    StatusRead,
}

func (s *Status) Capture(str []string) error {
	val, ok := statusMap[str[0]]
	if !ok {
		return fmt.Errorf("Unknown status '%s'", str[0])
	}
	*s = val
	return nil
}

func (s *Status) AsStatus() model.MergeReqStatus {
	switch *s {
	case StatusNew:
		return model.STATUS_NEW
	case StatusUpdated:
		return model.STATUS_UPDATED
	case StatusOld:
		return model.STATUS_OLD
	case StatusRead:
		return model.STATUS_READ
	}
	panic("Unexpected status")
}

//  Expression: Term ( Operator Term)*
//  Term: Match | SubExpression
//  SubExpresion: '(' Expression ')'
//  Match: (Not) Field Value

type Expression struct {
	Left  *Term           `@@`
	Right []*OperatorTerm `{ @@ }`
}

type OperatorTerm struct {
	Operator BinaryOperator `@("&" | "|")`
	Term     *Term          `@@`
}

type Term struct {
	Operator      *UnaryOperator `@("!")?`
	Match         *Match         `"~" @@`
	SubExpression *Expression    `| "(" @@ ")"`
}

type Match struct {
	Project  *String `"p" @@`
	RealName *String `| "n" @@`
	UserName *String `| "u" @@`
	Age      *Time   `| "a" @@`
	Activity *Time   `| "t" @@`
	State    *State  `| "s" @("o" | "c" | "m" | "l" |
                                           "open" | "closed" | "merged" | "locked")`
	Status *Status `| "m" @("n" | "u" | "o" | "r" |
                                "new" | "updated" | "old" | "read")`
	Label *String `| "l" @@`
}

type String struct {
	Bare         *string `@BareString`
	SingleQuoted *string `| @SingleQuotedString`
	DoubleQuoted *string `| @DoubleQuotedString`
}

func (t *String) AsString() string {
	if t.Bare != nil {
		return *t.Bare
	} else if t.SingleQuoted != nil {
		return (*t.SingleQuoted)[1 : len(*t.SingleQuoted)-1]
	} else if t.DoubleQuoted != nil {
		return (*t.DoubleQuoted)[1 : len(*t.DoubleQuoted)-1]
	}
	panic("missing string data")
}

type Time struct {
	Value int      `@Int`
	Unit  TimeUnit `@("y" | "o" | "w" | "d"| "h" | "m" | "s" | "mon" | "min" | "sec" |
                          "year" | "month" | "week" | "day" | "hour" | "minute" | "second")`
}

func (t *Time) AsDuration() time.Duration {
	return time.Duration(t.Value) * timeunitDuration[t.Unit]
}

func (m *Match) BuildFilter() model.MergeReqFilter {
	if m.Project != nil {
		return model.MergeReqFilterProject(m.Project.AsString(), true, true)
	} else if m.RealName != nil {
		return model.MergeReqFilterSubmitterRealName(m.RealName.AsString(), true, true)
	} else if m.UserName != nil {
		return model.MergeReqFilterSubmitterUserName(m.UserName.AsString(), true, true)
	} else if m.Age != nil {
		return model.MergeReqFilterAge(m.Age.AsDuration())
	} else if m.Activity != nil {
		return model.MergeReqFilterActivity(m.Activity.AsDuration())
	} else if m.State != nil {
		return model.MergeReqFilterState(m.State.AsState())
	} else if m.Status != nil {
		return model.MergeReqFilterMetadataStatus(m.Status.AsStatus())
	} else if m.Label != nil {
		return model.MergeReqFilterLabels(m.Label.AsString())
	}
	panic("Missing match value")
}

func (t *Term) BuildFilter() model.MergeReqFilter {
	if t.Match != nil {
		if t.Operator != nil && *t.Operator == UnaryOperatorReject {
			return model.MergeReqFilterReject(t.Match.BuildFilter())
		} else {
			return t.Match.BuildFilter()
		}
	} else if t.SubExpression != nil {
		if t.Operator != nil && *t.Operator == UnaryOperatorReject {
			return model.MergeReqFilterReject(t.SubExpression.BuildFilter())
		} else {
			return t.SubExpression.BuildFilter()
		}
	}
	panic("Missing term match / subexpression")
}

func (e *Expression) BuildFilter() model.MergeReqFilter {
	left := e.Left.BuildFilter()
	for _, right := range e.Right {
		switch right.Operator {
		case BinaryOperatorEither:
			left = model.MergeReqFilterEither(left,
				right.Term.BuildFilter())
		case BinaryOperatorBoth:
			left = model.MergeReqFilterBoth(left,
				right.Term.BuildFilter())
		default:
			panic("Unexpected operator")
		}
	}
	return left
}

func NewExpression(filter string) (*Expression, error) {
	lxr := lexer.Must(ebnf.New(`
            BareString = nondigit { nondigit | digit } .
            DoubleQuotedString = "\"" { "\u0000"…"\uffff"-"\"" } "\"" .
            SingleQuotedString = "'" { "\u0000"…"\uffff"-"'" } "'" .
            Int = (digit) { digit } .
            Whitespace = " " | "\t" | "\n" | "\r" .
            Punct = "~" | "&" | "|" | "!" | "(" | ")".

            nondigit = "a"…"z" | "A"…"Z" | "-" | "/" | "*" | "." | ":" | "[" | "]" | "?" .
            digit = "0"…"9" .
        `))
	p := participle.MustBuild(&Expression{},
		participle.Lexer(lxr),
		participle.Elide("Whitespace"),
	)

	expr := &Expression{}
	err := p.ParseString(filter, expr)
	if err != nil {
		return nil, err
	}
	return expr, nil
}
