// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package net

import (
	"crypto/tls"
	"fmt"
	"io"
	"net"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"
)

var logdir string

func SetHTTPLogDir(dir string) {
	logdir = dir
}

type filteredWriter struct {
	io.Writer

	Header []byte
	Filter *regexp.Regexp
}

func newFilteredWriter(writer io.Writer) *filteredWriter {
	return &filteredWriter{
		Writer: writer,
		Filter: regexp.MustCompile("Private-Token: \\w{20}"),
	}
}

func (writer *filteredWriter) Write(p []byte) (int, error) {
	if writer.Filter != nil {
		writer.Header = append(writer.Header, p...)

		if writer.Filter.Match(writer.Header) {
			data := writer.Filter.ReplaceAll(p,
				[]byte("Private-Token: "+strings.Repeat("*", 20)))
			n, err := writer.Writer.Write(data)
			writer.Header = []byte{}
			writer.Filter = nil
			if n != len(p) {
				return 0, fmt.Errorf("Short write while logging")
			}
			return len(p), err
		}
		return len(p), nil
	}

	return writer.Writer.Write(p)
}

type connectionLogger struct {
	net.Conn
	io.Reader
	io.Writer
}

func newConnectionLogger(c net.Conn, output io.Writer) net.Conn {
	return &connectionLogger{
		Conn:   c,
		Reader: io.TeeReader(c, output),
		Writer: io.MultiWriter(newFilteredWriter(output), c),
	}
}

func (conn *connectionLogger) Read(b []byte) (int, error) {
	return conn.Reader.Read(b)
}

func (conn *connectionLogger) Write(b []byte) (int, error) {
	return conn.Writer.Write(b)
}

func dialLogger(network, address string) (net.Conn, error) {
	dial := net.Dialer{
		Timeout: 5 * time.Second,
	}
	conn, err := dial.Dial(network, address)
	if err != nil {
		return nil, err
	}

	err = os.MkdirAll(logdir, 0700)
	if err != nil {
		return nil, err
	}

	logfile := filepath.Join(logdir, fmt.Sprintf("bichon-http-%s-%d.log", address, time.Now().UnixNano()))

	output, err := os.Create(logfile)
	if err != nil {
		return nil, err
	}

	return newConnectionLogger(conn, output), nil
}

func dialTLSLogger(network, address string, tlscfg *tls.Config) (net.Conn, error) {
	dial := net.Dialer{
		Timeout: 5 * time.Second,
	}
	conn, err := dial.Dial(network, address)
	if err != nil {
		return nil, err
	}

	addr, err := url.Parse(fmt.Sprintf("https://%s", address))
	if err != nil {
		return nil, err
	}

	var newtlscfg tls.Config
	if tlscfg != nil {
		newtlscfg = *tlscfg
	}
	serverName := addr.Host[:strings.LastIndex(addr.Host, ":")]
	newtlscfg.ServerName = serverName

	tlsConn := tls.Client(conn, &newtlscfg)

	errc := make(chan error, 2)
	timer := time.AfterFunc(10*time.Second, func() {
		errc <- fmt.Errorf("TLS handshake timeout for %s", address)
	})
	go func() {
		err := tlsConn.Handshake()
		timer.Stop()
		errc <- err
	}()

	err = <-errc
	if err != nil {
		conn.Close()
		return nil, err
	}

	err = tlsConn.VerifyHostname(newtlscfg.ServerName)
	if err != nil {
		conn.Close()
		return nil, err
	}

	err = os.MkdirAll(logdir, 0700)
	if err != nil {
		return nil, err
	}

	logfile := filepath.Join(logdir, fmt.Sprintf("bichon-http-%s-%d.log", address, time.Now().UnixNano()))

	output, err := os.Create(logfile)
	if err != nil {
		return nil, err
	}

	return newConnectionLogger(tlsConn, output), nil
}

func NewHTTPClient(tlscfg *tls.Config) *http.Client {
	if logdir == "" {
		return &http.Client{
			Timeout: time.Second * 10,
			Transport: &http.Transport{
				Dial: (&net.Dialer{
					Timeout: 5 * time.Second,
				}).Dial,
				TLSClientConfig:     tlscfg,
				TLSHandshakeTimeout: 10 * time.Second,
			},
		}
	} else {
		return &http.Client{
			Timeout: time.Second * 10,
			Transport: &http.Transport{
				DisableKeepAlives:  true,
				DisableCompression: true,
				Dial:               dialLogger,
				DialTLS: func(network, address string) (net.Conn, error) {
					return dialTLSLogger(network, address, tlscfg)
				},
			},
		}
	}
}
