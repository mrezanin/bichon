// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	"testing"
	"time"
)

func TestMergeReqJSON(t *testing.T) {
	v1 := Series{
		Index:   34643,
		Version: 1,
		Patches: []Commit{
			Commit{
				Hash:  "b7e0c7e4ca23c40c13fc7dee3aed4f8a33af48b6",
				Title: "Do something",
				Author: User{
					Name:  "Fred",
					Email: "fred@example.org",
				},
				CreatedAt: time.Date(2019, 4, 14, 8, 15, 2, 0, time.UTC),
				Committer: User{
					Name:  "Emily",
					Email: "emily@example.org",
				},
				UpdatedAt: time.Date(2019, 4, 16, 07, 45, 21, 0, time.UTC),
				Message:   "A detailed message",
			},
		},
	}
	v2 := Series{
		Index:   3532,
		Version: 2,
		Patches: []Commit{
			Commit{
				Hash:  "b7e0c7e4ca23c40c13fc7dee3aed4f8a33af48b6",
				Title: "Do something",
				Author: User{
					Name:  "Fred",
					Email: "fred@example.org",
				},
				CreatedAt: time.Date(2019, 4, 14, 8, 15, 2, 0, time.UTC),
				Committer: User{
					Name:  "Emily",
					Email: "emily@example.org",
				},
				UpdatedAt: time.Date(2019, 4, 16, 7, 45, 21, 0, time.UTC),
				Message:   "A detailed message",
			},
			Commit{
				Hash:  "5dfeb6cc6c3034b7c56eacacec1f50ee9b8edf1f",
				Title: "Do something",
				Author: User{
					Name:  "Fred",
					Email: "fred@example.org",
				},
				CreatedAt: time.Date(2019, 4, 17, 8, 16, 24, 0, time.UTC),
				Committer: User{
					Name:  "Emily",
					Email: "emily@example.org",
				},
				UpdatedAt: time.Date(2019, 4, 17, 8, 45, 25, 0, time.UTC),
				Message:   "A detailed message",
			},
		},
	}

	mreq := MergeReq{
		ID:        8,
		Title:     "Implement some stuff",
		CreatedAt: time.Date(2019, 4, 24, 15, 24, 23, 0, time.UTC),
		UpdatedAt: time.Date(2019, 4, 28, 12, 54, 17, 0, time.UTC),
		Submitter: Account{
			UserName: "fred",
			RealName: "Fred",
		},
		Description: "Do some stuff",
		Versions: []Series{
			v1, v2,
		},
		SourceBranch: "fishfood",
		TargetBranch: "master",
	}

	expected := "{\n" +
		"  \"id\": 8,\n" +
		"  \"title\": \"Implement some stuff\",\n" +
		"  \"createdAt\": \"2019-04-24T15:24:23Z\",\n" +
		"  \"updatedAt\": \"2019-04-28T12:54:17Z\",\n" +
		"  \"submitter\": {\n" +
		"    \"username\": \"fred\",\n" +
		"    \"realname\": \"Fred\"\n" +
		"  },\n" +
		"  \"assignee\": null,\n" +
		"  \"description\": \"Do some stuff\",\n" +
		"  \"versions\": [\n" +
		"    {\n" +
		"      \"index\": 34643,\n" +
		"      \"version\": 1,\n" +
		"      \"baseHash\": \"\",\n" +
		"      \"startHash\": \"\",\n" +
		"      \"headHash\": \"\",\n" +
		"      \"patches\": [\n" +
		"        {\n" +
		"          \"hash\": \"b7e0c7e4ca23c40c13fc7dee3aed4f8a33af48b6\",\n" +
		"          \"title\": \"Do something\",\n" +
		"          \"author\": {\n" +
		"            \"name\": \"Fred\",\n" +
		"            \"email\": \"fred@example.org\"\n" +
		"          },\n" +
		"          \"committer\": {\n" +
		"            \"name\": \"Emily\",\n" +
		"            \"email\": \"emily@example.org\"\n" +
		"          },\n" +
		"          \"createdAt\": \"2019-04-14T08:15:02Z\",\n" +
		"          \"updatedAt\": \"2019-04-16T07:45:21Z\",\n" +
		"          \"message\": \"A detailed message\",\n" +
		"          \"diffs\": null,\n" +
		"          \"bichonMetadata\": {\n" +
		"            \"partial\": false\n" +
		"          }\n" +
		"        }\n" +
		"      ],\n" +
		"      \"bichonMetadata\": {\n" +
		"        \"partial\": false\n" +
		"      }\n" +
		"    },\n" +
		"    {\n" +
		"      \"index\": 3532,\n" +
		"      \"version\": 2,\n" +
		"      \"baseHash\": \"\",\n" +
		"      \"startHash\": \"\",\n" +
		"      \"headHash\": \"\",\n" +
		"      \"patches\": [\n" +
		"        {\n" +
		"          \"hash\": \"b7e0c7e4ca23c40c13fc7dee3aed4f8a33af48b6\",\n" +
		"          \"title\": \"Do something\",\n" +
		"          \"author\": {\n" +
		"            \"name\": \"Fred\",\n" +
		"            \"email\": \"fred@example.org\"\n" +
		"          },\n" +
		"          \"committer\": {\n" +
		"            \"name\": \"Emily\",\n" +
		"            \"email\": \"emily@example.org\"\n" +
		"          },\n" +
		"          \"createdAt\": \"2019-04-14T08:15:02Z\",\n" +
		"          \"updatedAt\": \"2019-04-16T07:45:21Z\",\n" +
		"          \"message\": \"A detailed message\",\n" +
		"          \"diffs\": null,\n" +
		"          \"bichonMetadata\": {\n" +
		"            \"partial\": false\n" +
		"          }\n" +
		"        },\n" +
		"        {\n" +
		"          \"hash\": \"5dfeb6cc6c3034b7c56eacacec1f50ee9b8edf1f\",\n" +
		"          \"title\": \"Do something\",\n" +
		"          \"author\": {\n" +
		"            \"name\": \"Fred\",\n" +
		"            \"email\": \"fred@example.org\"\n" +
		"          },\n" +
		"          \"committer\": {\n" +
		"            \"name\": \"Emily\",\n" +
		"            \"email\": \"emily@example.org\"\n" +
		"          },\n" +
		"          \"createdAt\": \"2019-04-17T08:16:24Z\",\n" +
		"          \"updatedAt\": \"2019-04-17T08:45:25Z\",\n" +
		"          \"message\": \"A detailed message\",\n" +
		"          \"diffs\": null,\n" +
		"          \"bichonMetadata\": {\n" +
		"            \"partial\": false\n" +
		"          }\n" +
		"        }\n" +
		"      ],\n" +
		"      \"bichonMetadata\": {\n" +
		"        \"partial\": false\n" +
		"      }\n" +
		"    }\n" +
		"  ],\n" +
		"  \"threads\": null,\n" +
		"  \"state\": \"\",\n" +
		"  \"labels\": null,\n" +
		"  \"upVotes\": 0,\n" +
		"  \"downVotes\": 0,\n" +
		"  \"mergeStatus\": \"\",\n" +
		"  \"mergeAfterPipeline\": false,\n" +
		"  \"sourceBranch\": \"fishfood\",\n" +
		"  \"targetBranch\": \"master\",\n" +
		"  \"bichonMetadata\": {\n" +
		"    \"partial\": false,\n" +
		"    \"status\": \"\"\n" +
		"  }\n" +
		"}"
	actual, err := mreq.ToJSON()
	if err != nil {
		t.Fatal(err)
	}

	if string(actual) != expected {
		t.Errorf("Expected '%s' but got '%s'", expected, actual)
	}

	newmreq, err := NewMergeReqFromJSON(actual)
	if err != nil {
		t.Fatal(err)
	}

	actual2, err := newmreq.ToJSON()
	if err != nil {
		t.Fatal(err)
	}

	if string(actual2) != string(actual) {
		t.Errorf("Expected '%s' but got '%s'", actual2, actual)
	}

}
