// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/tview"
)

type SortFormListener interface {
	SortFormConfirm(model.MergeReqSorter)
	SortFormCancel()
}

type SortFormOrder int

const (
	SORT_FORM_ORDER_PROJECT_ID SortFormOrder = iota
	SORT_FORM_ORDER_AGE
	SORT_FORM_ORDER_ACTIVITY
	SORT_FORM_ORDER_TITLE
	SORT_FORM_ORDER_AUTHOR
)

type SortForm struct {
	tview.Primitive

	Form     *tview.Form
	Listener SortFormListener

	Order   SortFormOrder
	Reverse bool
}

func (form *SortForm) cancelFunc() {
	form.Form.SetFocus(0)
	form.Listener.SortFormCancel()
}

func NewSortForm(listener SortFormListener, order SortFormOrder, reverse bool) *SortForm {

	form := &SortForm{
		Form:     tview.NewForm(),
		Listener: listener,
		Order:    order,
		Reverse:  reverse,
	}

	form.Primitive = Modal(form.Form, 30, 9)

	form.Form.SetCancelFunc(form.cancelFunc)

	form.Form.SetBorder(true).
		SetTitle("Merge request sorting")
	form.Form.AddDropDown("Field",
		[]string{
			"Project",
			"Age",
			"Activity",
			"Title",
			"Author",
		},
		int(order),
		func(val string, idx int) {
			form.Order = SortFormOrder(idx)
		})
	form.Form.AddCheckbox("Reverse", reverse,
		func(checked bool) {
			form.Reverse = checked
		})

	form.Form.AddButton("Apply", func() {
		form.Form.SetFocus(0)
		sorter := form.GetMergeReqSorter()
		form.Listener.SortFormConfirm(sorter)
	})

	form.Form.AddButton("Cancel", form.cancelFunc)

	return form
}

func (form *SortForm) GetMergeReqSorter() model.MergeReqSorter {
	sorter := model.MergeReqSorterBoth(
		model.MergeReqSorterRepo,
		model.MergeReqSorterID)
	switch form.Order {
	case SORT_FORM_ORDER_PROJECT_ID:
		log.Info("Sort project ID")
	case SORT_FORM_ORDER_AGE:
		log.Info("Sort age")
		sorter = model.MergeReqSorterBoth(
			model.MergeReqSorterAge,
			sorter)
	case SORT_FORM_ORDER_ACTIVITY:
		log.Info("Sort activity")
		sorter = model.MergeReqSorterBoth(
			model.MergeReqSorterActivity,
			sorter)
	case SORT_FORM_ORDER_TITLE:
		log.Info("Sort title")
		sorter = model.MergeReqSorterBoth(
			model.MergeReqSorterTitle,
			sorter)
	case SORT_FORM_ORDER_AUTHOR:
		log.Info("Sort author")
		sorter = model.MergeReqSorterBoth(
			model.MergeReqSorterSubmitter,
			sorter)
	}

	if form.Reverse {
		log.Info("Sort reverse")
		sorter = model.MergeReqSorterReverse(sorter)
	}

	return sorter
}
