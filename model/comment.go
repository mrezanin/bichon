// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
)

type Comment struct {
	Author      User      `json:"author"`
	CreatedAt   time.Time `json:"createdAt"`
	UpdatedAt   time.Time `json:"updatedt"`
	Description string    `json:"description"`
	System      bool      `json:"system"`

	Context *CommentContext `json:"context"`
}

type CommentThread struct {
	ID         string    `json:"id"`
	Individual bool      `json:"individual"`
	Comments   []Comment `json:"comments"`
}

type CommentContext struct {
	BaseHash  string `json:"baseHash"`
	StartHash string `json:"startHash"`
	HeadHash  string `json:"headHash"`
	NewFile   string `json:"newFile"`
	NewLine   uint   `json:"newLine"`
	OldFile   string `json:"oldFile"`
	OldLine   uint   `json:"oldLine"`
}

func (ctx *CommentContext) String() string {
	return fmt.Sprintf("%s %s:%d %s:%d", ctx.HeadHash, ctx.OldFile, ctx.OldLine, ctx.NewFile, ctx.NewLine)
}

func (ctx *CommentContext) Rebase(target *Commit, series *Series) *CommentContext {
	log.Infof("Rebasing %s to %s", ctx.String(), target.Hash)

	newCtx := *ctx

	initialPatch := -1
	targetPatch := -1
	for idx, commit := range series.Patches {
		log.Infof("Compare to %s", commit.Hash)
		if newCtx.HeadHash == commit.Hash {
			initialPatch = idx
		}
		if target.Hash == commit.Hash {
			targetPatch = idx
		}
	}

	if initialPatch == -1 {
		log.Infof("Commit %s is not present in series", newCtx.HeadHash)
		return nil
	}
	if targetPatch == -1 {
		log.Infof("Commit %s is not present in series", target.Hash)
		return nil
	}

	log.Infof("Identified initial patch %d target patch %d", initialPatch, targetPatch)
	if initialPatch < targetPatch {
		log.Infof("Comment was on %d/%d (%s) before the target patch %d/%d",
			initialPatch, len(series.Patches), newCtx.HeadHash, targetPatch, len(series.Patches))
		return nil
	}

	for i := initialPatch; i > targetPatch; i-- {
		commit := series.Patches[i]
		diff := commit.GetFile(newCtx.NewFile)
		if diff == nil {
			continue
		}
		if newCtx.OldLine != 0 {
			if i != initialPatch {
				break
			}
			newCtx.NewLine = newCtx.OldLine
			newCtx.OldLine = 0
			newCtx.HeadHash = series.Patches[i-1].Hash
		} else {
			// XXX should deal with file renames ?
			origLine, err := diff.Revert(newCtx.NewLine)
			if err != nil {
				return nil
			}
			if origLine == 0 {
				return nil
			}
			newCtx.NewLine = origLine
			newCtx.HeadHash = series.Patches[i-1].Hash
			if diff.RenamedFile {
				log.Infof("Renamed from %s to %s", newCtx.NewFile, diff.OldFile)
				newCtx.NewFile = diff.OldFile
			}
		}
	}

	log.Infof("Rebased %s to %s", newCtx.String(), target.Hash)

	return &newCtx
}
