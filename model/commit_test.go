// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2020 Red Hat, Inc.

package model

import (
	"testing"
)

func TestDiffRevert(t *testing.T) {
	content := `@@ -457,6 +457,9 @@ func (page *DetailPage) HandleInput(event *tcell.EventKey) *tcell.EventKey {
        case tcell.KeyLeft:
                if !page.CommentEdit {
                        row, col := page.Patches.GetSelection()
+                       if row < 0 {
+                               break
+                       }
                        if row > 0 {
                                page.Patches.Select(row-1, col)
                        }
@@ -469,6 +472,9 @@ func (page *DetailPage) HandleInput(event *tcell.EventKey) *tcell.EventKey {
        case tcell.KeyRight:
                if !page.CommentEdit {
                        row, col := page.Patches.GetSelection()
+                       if row < 0 {
+                               break
+                       }
                        if row < (page.Patches.GetRowCount() - 1) {
                                page.Patches.Select(row+1, col)
                        }
@@ -608,7 +614,7 @@ func (page *DetailPage) switchPatch(row, col int) {
        }
 
        log.Infof("Re-rendering patches %d", row)
-       if row == 0 {
+       if row <= 0 {
                page.Patch = nil
                page.ContentLen = 0
                page.ContentRegions = []PatchRegion{}
`

	diff := Diff{
		Content:     content,
		OldFile:     "view/detail.go",
		NewFile:     "view/detail.go",
		OldMode:     "100644",
		NewMode:     "100644",
		CreatedFile: false,
		RenamedFile: false,
		DeletedFile: false,
	}

	data := make(map[uint]uint)
	data[100] = 100
	data[458] = 458
	data[464] = 461
	data[619] = 613
	data[700] = 694

	for lineIn, lineOut := range data {
		newLine, err := diff.Revert(lineIn)
		if err != nil {
			t.Errorf("Failure rebasing line %d to %d", lineIn, lineOut)
		}

		if newLine != lineOut {
			t.Errorf("Expected rebase line %d to %d not %d", lineIn, lineOut, newLine)
		}
	}
}
