// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, package.

package model

import (
	"sort"
	"strings"
)

// Return true if a is less than b
type MergeReqSorter func(a, b MergeReq) bool

type mergeReqSorter struct {
	Data   []*MergeReq
	Sorter MergeReqSorter
}

func (sorter *mergeReqSorter) Len() int {
	return len(sorter.Data)
}

func (sorter *mergeReqSorter) Less(i, j int) bool {
	return sorter.Sorter(*sorter.Data[i], *sorter.Data[j])
}

// Swap swaps the elements with indexes i and j.
func (sorter *mergeReqSorter) Swap(i, j int) {
	mreq := sorter.Data[i]
	sorter.Data[i] = sorter.Data[j]
	sorter.Data[j] = mreq
}

func MergeReqSort(mreqs []*MergeReq, sorter MergeReqSorter) []*MergeReq {
	if sorter == nil {
		sorter = MergeReqSorterBoth(MergeReqSorterRepo, MergeReqSorterID)
	}
	mreqsorter := &mergeReqSorter{
		Data:   mreqs,
		Sorter: sorter,
	}

	sort.Sort(mreqsorter)

	return mreqsorter.Data
}

func MergeReqSorterID(a, b MergeReq) bool {
	return a.ID < b.ID
}

func MergeReqSorterTitle(a, b MergeReq) bool {
	return strings.ToLower(a.Title) < strings.ToLower(b.Title)
}

func MergeReqSorterReverse(this MergeReqSorter) MergeReqSorter {
	return func(a, b MergeReq) bool {
		return !this(a, b)
	}
}

func MergeReqSorterBoth(this, that MergeReqSorter) MergeReqSorter {
	return func(a, b MergeReq) bool {
		if this(a, b) {
			return true
		}
		if this(b, a) {
			return false
		}
		return that(a, b)
	}
}

func MergeReqSorterAge(a, b MergeReq) bool {
	return a.CreatedAt.After(b.CreatedAt)
}

func MergeReqSorterActivity(a, b MergeReq) bool {
	return a.UpdatedAt.After(b.UpdatedAt)
}

func MergeReqSorterSubmitter(a, b MergeReq) bool {
	return strings.ToLower(a.Submitter.RealName) < strings.ToLower(b.Submitter.RealName)
}

func MergeReqSorterRepo(a, b MergeReq) bool {
	return a.Repo.Project < b.Repo.Project
}
