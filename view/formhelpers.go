// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019-2020 Red Hat, Inc.

package view

import (
	"gitlab.com/bichon-project/tview"
)

func addInputField(form *tview.Form, label, value string, width int, readonly bool) *tview.InputField {
	form.AddInputField(label, value, width, nil, nil)

	item := form.GetFormItemByLabel(label)

	input, _ := item.(*tview.InputField)

	input.SetReadOnly(readonly)

	return input
}

func addCheckbox(form *tview.Form, label string, message string, value bool, readonly bool) *tview.Checkbox {
	form.AddCheckbox(label, value, nil)

	item := form.GetFormItemByLabel(label)

	input, _ := item.(*tview.Checkbox)

	input.SetReadOnly(readonly)
	input.SetMessage(message)

	return input
}

func addDropDown(form *tview.Form, label string, options []string, readonly bool) *tview.DropDown {
	form.AddDropDown(label, options, 0, nil)

	item := form.GetFormItemByLabel(label)

	input, _ := item.(*tview.DropDown)

	input.SetReadOnly(readonly)

	return input
}
